var gulp = require('gulp');				//NURODOM KAD NAUDOSIM GULP
var sass = require('gulp-sass');			//NURODOM KAD NAUDOSIM SASS
var imagemin = require('gulp-imagemin');


// GULP UZDUOTIS, KURI TIKRINA ASSETS FOLDERI,
// JEI TEN YRA SCSS FAILAS, GULP JI STEBI IR UZSAUGOJUS SCSS FAILA
// JIS JI KOMPILIUOJA IS SCSS I CSS 
gulp.task('styles', function() {
	gulp.src('resources/sass/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('assets/css/'))
});


// NURODOM KAS VYKST JEI KOMANDINEI EILUTEJ PARASYSIM GULP
gulp.task('default', function() {
	gulp.watch('resources/**/*.scss', ['styles']);
});

// IMAGEMIN---------////////

gulp.task('image', function() {
	gulp.src('resources/img/*')
		.pipe(imagemin())
		.pipe(gulp.dest('assets/img'));
	});






