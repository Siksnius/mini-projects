$(document).ready(function() {

    var trackLength = 500;

     function calcRacerTime(weight, age) {
      return (weight/trackLength*age)*(Math.random() * 0.9 + Math.random());
            }

            function random(min, max){
                    return Math.floor(Math.random() * (max - min + 1)) + min;
                }


            var racer = function(name, weight, age) {
                this.name = name;
                this.weight = weight;
                this.age = age;
                
                this.time = parseFloat(calcRacerTime(this.weight, this.age).toFixed(3));
                }

                var racersArr = [];




                var batmanas = new racer('Oleg', random(50, 100), random(25, 75));
                racersArr.push(batmanas);

                var human = new racer('Sergej', random(50, 100), random(25, 75));
                racersArr.push(human);

                var nindze = new racer('Anatolij', (50, 100), random(25, 75));
                racersArr.push(nindze);


                console.log(batmanas);
                console.log(human);
                console.log(nindze);

                $('#startRace').on('click', function() {
                    var batmanasRacer = $('.batman');
                    var humanRacer = $('.human');
                    var nindzeRacer = $('.nindze');

                    batmanasRacer.css({
                        'transition': batmanas.time + 's',
                        'left': window.innerWidth - 350 + 'px'
                    });

                       humanRacer.css({
                        'transition': human.time + 's',
                        'left': window.innerWidth - 350 + 'px'
                    });

                       nindzeRacer.css({
                        'transition': nindze.time + 's',
                        'left': window.innerWidth - 350 + 'px'
                    });


                       var num = findWinner();

                       $('#winner').text(racersArr[num].time + " " + racersArr[num].name);
                    



                });

                function findWinner(){
                    var winner = 0;
                    var bestTime = racersArr[winner].time;


                    for(var i = 1; i < racersArr.length; i++) {
                        if(racersArr[i].time < bestTime) {
                            winner = i;
                            besTime =racersArr[i].time;


                        }

                    }

                    return winner;
                }


                $('#resetRace').on('click', function(){
                    location.href = 'index.html';

                });

            });